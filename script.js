//задача 1

let array = [NaN, 1, 2, false, 3, '',undefined, 4, null];

function filtered(arr) {
    return arr.filter(function(i) { return !!i; });
}

console.log(filtered(array));


//задача 2

function lengthOfStrings(strings = []) {
    return strings.map((value) => value.length)
}
console.log(lengthOfStrings(['dkdk', 'djkgk', 'sdfsdf']))


//задача 3

function move(array, from = 0, to = 1) {
     const elfrom = array[from]
     const elto = array[to]

     array[from] = elto
     array[to] = elfrom

}

let a = [1, 2, 3]

move(a)
console.log(a)



